import numpy as np
import cv2


def video():
    cap = cv2.VideoCapture(0)
    count = 0
    points = []

    while True:

        ret, frame = cap.read()

        # cv2.imshow("Video", frame)
        # frame = cv2.imread("media/Contour.png")

        frameGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        ret,thresh1 = cv2.threshold(frameGray,240,255,cv2.THRESH_BINARY)
        contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        cv2.drawContours(frame, contours, -1, (0,255,0), 3)
            
        for cnt in contours:
            M = cv2.moments(cnt)
            cx = int(M['m10']/(1e-5 + M['m00']))
            cy = int(M['m01']/(1e-5 + M['m00']))
            cv2.circle(frame, (cx,cy), 10, (255,0,0), -1)
            
        cv2.imshow("Thresh", frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            if len(contours) == 1:
                points.append((cx,cy))
                print(cx,cy)
            else: 
                print("Invalid contour number")
        
        if cv2.waitKey(1) & 0xFF == 27:
            break

    cap.release()

    with open('coords.txt', 'w') as fp:
        fp.write('\n'.join('%s %s' % x for x in points))

video()
